package me.krawtschenko.yulia.view;

import java.io.Serializable;
import java.util.TreeMap;
import java.util.Map;
import java.util.Scanner;

public class View {
    protected Scanner input;
    protected TreeMap<MenuOption, MenuItem> menu;

    public View() {
        OptionKeyComparator keyComparator= new OptionKeyComparator();

        input = new Scanner(System.in, "UTF-8");
        menu = new TreeMap<>(keyComparator);

        MenuItem quit = new MenuItem(MenuOption.QUIT, "Quit");
        menu.put(MenuOption.QUIT, quit);
    }

    public void displayMenu() {
        System.out.println("MENU");

        for (Map.Entry<MenuOption, MenuItem> option : menu.entrySet()) {
            System.out.printf("%s - %s%n", option.getKey().getKeyToPress(),
                    option.getValue().getDescription());
        }

        MenuOption chosen;
        do {
            chosen = awaitValidMenuOption();
        } while (!chosen.equals(MenuOption.QUIT.getKeyToPress()));
    }

    public MenuOption awaitValidMenuOption() {
        String choice;
        MenuOption chosen;
        do {
            choice = input.nextLine().toUpperCase();
            chosen = MenuOption.get(choice);
        } while (!menu.containsKey(chosen));

        menu.get(chosen).getAction().execute();

        return chosen;
    }

    public void addMenuItemAction(MenuOption key, MenuAction action) {
        MenuItem item = menu.get(key);
        item.linkAction(action);
        menu.put(key, item);
    }

    public void printMessage(String message) {
        System.out.println(message);
    }

    public void print(Serializable contents) {
        System.out.println(contents.toString());
    }
}
