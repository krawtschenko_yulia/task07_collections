package me.krawtschenko.yulia.view;

public class ApplicationView extends View{
    public ApplicationView() {
        super();

        MenuItem seeTreeDemo = new MenuItem(MenuOption.SHOW_TREE_DEMO,
                "See BST demo");
        super.menu.put(MenuOption.SHOW_TREE_DEMO, seeTreeDemo);
    }

    public String requestSearchQuery() {
        String query = "";

        while (query.isBlank()) {
            System.out.println("Search for: ");
            query = input.nextLine();
        }

        return query;
    }

    public void displayFound(String found) {
        System.out.print("Found: ");
        System.out.println(found);
    }
}
