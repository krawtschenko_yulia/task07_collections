package me.krawtschenko.yulia.model;

import java.io.Serializable;

public class Tree<T> implements Serializable {
    private Node<T> root;

    public String get(int key) {
        Node<T> current = root;

        while (current != null && key != current.getKey()) {
            if (key < current.getKey()) {
                current = current.getLeftChild();
            } else {
                current = current.getRightChild();
            }
        }

        return current.toString();
    }

    public void put(int key, T data) {
        Node<T> newNode = new Node(key, data);

        if (root == null) {
            root = newNode;
            return;
        }

        Node<T> current = root;
        Node<T> parent;

        while(true) {
            parent = current;

            if (key < current.getKey()) {
                current = current.getLeftChild();
                if (current == null) {
                    parent.setLeftChild(newNode);
                    return;
                }
            } else {
                current = current.getRightChild();

                if (current == null) {
                    parent.setRightChild(newNode);
                    return;
                }
            }
        }
    }

    public boolean remove(int key) {
        Node<T> current = root;
        Node<T> parent = null;
        boolean isLeftChild = false;

        while(current != null && key != current.getKey()) {
            parent = current;

            if (key < current.getKey()) {
                current = current.getLeftChild();
                isLeftChild = true;
            } else {
                current = current.getLeftChild();
                isLeftChild = false;
            }
        }

        if (current == null) {
            return false;
        }

        if (current.getLeftChild() == null && current.getRightChild() == null) {
            if (current.equals(root)) {
                root = null;
                return true;
            }

            if (isLeftChild) {
                parent.setLeftChild(null);
            } else {
                parent.setRightChild(null);
            }
        }

        if (current.getLeftChild() != null && current.getRightChild() == null) {
            if (current.equals(root)) {
                root = current.getLeftChild();
                return true;
            }

            if (isLeftChild) {
                parent.setLeftChild(current.getLeftChild());
            } else {
                parent.setRightChild(current.getLeftChild());
            }
        }

        if (current.getLeftChild() == null && current.getRightChild() != null) {
            if (current.equals(root)) {
                root = current.getRightChild();
                return true;
            }

            if (isLeftChild) {
                parent.setLeftChild(current.getRightChild());
            } else {
                parent.setRightChild(current.getRightChild());
            }
        }

        if (current.getLeftChild() != null && current.getRightChild() != null) {
            Node<T> successor = findSuccessor(current);

            if (current.equals(root)) {
                root = successor;
            } else if (isLeftChild) {
                parent.setLeftChild(successor);
            } else {
                parent.setRightChild(successor);
            }
            successor.setLeftChild(current.getLeftChild());
        }
        return true;
    }

    public String toString() {
        if (root == null) {
            return root.toString();
        }
        return print(root);
    }

    private String print(Node<T> localRoot) {
        String output = "";

        if (localRoot != null) {
            output += localRoot.toString();
            output += " ";

            output += print(localRoot.getLeftChild());
            output += print(localRoot.getRightChild());
        }

        return output;
    }

    private Node<T> findSuccessor(Node<T> deleted) {
        Node<T> successorParent = null;
        Node<T> successor = deleted;
        Node<T> current = deleted.getRightChild();

        while (current != null) {
            successorParent = successor;
            successor = current;
            current = current.getLeftChild();
        }

        if (successor == deleted.getRightChild()) {
            return successor;
        }

        successorParent.setLeftChild(successor.getRightChild());
        successor.setRightChild(deleted.getRightChild());

        return successor;
    }
}
