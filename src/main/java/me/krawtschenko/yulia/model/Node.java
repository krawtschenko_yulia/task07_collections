package me.krawtschenko.yulia.model;

class Node<T> {
    private int key;
    private T data;
    private Node<T> left;
    private Node<T> right;

    public Node(int key, T data, Node<T> left, Node<T> right) {
        this.key = key;
        this.data = data;
        this.left = left;
        this.right = right;
    }

    public Node(int key, T data) {
        this(key, data, null, null);
    }

    public String toString() {
        return data.toString();
    }

    public int getKey() {
        return key;
    }

    public T getData() {
        return data;
    }

    public Node<T> getLeftChild() {
        return left;
    }

    public void setLeftChild(Node<T> left) {
        this.left = left;
    }

    public Node<T> getRightChild() {
        return right;
    }

    public void setRightChild(Node<T> right) {
        this.right = right;
    }
}
