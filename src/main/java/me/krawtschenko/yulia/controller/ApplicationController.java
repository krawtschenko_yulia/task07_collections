package me.krawtschenko.yulia.controller;

import me.krawtschenko.yulia.model.Tree;
import me.krawtschenko.yulia.view.ApplicationView;
import me.krawtschenko.yulia.view.MenuAction;
import me.krawtschenko.yulia.view.MenuOption;

public class ApplicationController {
    private ApplicationView view;

    private MenuAction showTreeDemo;
    private MenuAction quit;

    public ApplicationController() {
        view = new ApplicationView();

        showTreeDemo = new MenuAction() {
            @Override
            public void execute() {
                Tree<String> tree = new Tree<>();

                tree.put(5, "five");
                tree.put(13, "thirteen");
                tree.put(2, "two");
                tree.put(7, "seven");
                tree.put(3, "three");
                tree.put(11, "eleven");

                view.printMessage("Tree created: ");
                view.print(tree);

                // Tree calls toString(); nothing to do with generics
                String found = tree.get(7);

                view.printMessage("Node with key 7: ");
                view.print(found);

                tree.remove(5);

                view.printMessage("Tree with node 5 deleted: ");
                view.print(tree);

            }
        };
        view.addMenuItemAction(MenuOption.SHOW_TREE_DEMO, showTreeDemo);


        quit = new MenuAction() {
            @Override
            public void execute() {
                System.exit(0);
            }
        };
        view.addMenuItemAction(MenuOption.QUIT, quit);
    }

    public void startMenu() {
        view.displayMenu();
    }
}